Snowcat is a private, fast, and beautiful web browser that's created with Electron and styled with Tailwind CSS. Enjoy a clean and unique experience while surfing the web.

In this age where many browsers track their users, it's nice to have a break from telemetry and browse privately. Snowcat never collects any data, and is 100% telemetry free!

New things are constantly being added to Snowcat, so be on the lookout for new releases. You can view some features and fixes that are in the works on [this board (coming soon)](https://github.com/).

## Development
```bash
# Clone the repo
git clone https://github.com/snowcatridge10/snowcat
cd snowcat

# Install the dependencies
npm install

# Start the app
npm run dev
# This runs `npm start` and `npm run tailwind` concurrently
```

## Building the app
```bash
# Clone the repo
git clone https://github.com/snowcatridge10/snowcat
cd snowcat

# Install the dependencies
npm install

# Make the app
npm run make

# The built app should be located at `./out/make/{filetype}/{architecture}/{filename}.{filetype}`
# Eg. `./out/make/deb/arm64/ninetails_1.0.0_arm64.deb`
```

## The Android app is coming soon

## Credits

- Ninetails
- snowcatridge10